Simple plugin for UE 5.0.3 that adds Blueprint-ready Editor Toast Notifications

<b>OVERVIEW</b>

All functionality is wrapped inside an EditorSubsystem, with the following methods:

- <b>NotifyError</b>: sends a toast notification for a given duration with a custom message. The notification also contains a small icon (Icons.Error)
- <b>NotifyInfo</b>: sends a toast notification for a given duration with a custom message. The notification also contains a small icon (Icons.Info)
- <b>NotifyWithCustomIcon</b>: similar to the previous both, but users can specify any icon on the avaliable Editor Styles.

<b>HOW TO INSTALL</b>

To install the plugin, you just need to download it with your favourite method, and then put the whole folder in the Plugins folder of your project
