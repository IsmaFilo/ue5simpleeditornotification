// MIT License
//
// Copyright (c) 2022 ISMA FILO
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include "CoreMinimal.h"
#include "EditorSubsystem.h"
#include "FTNotificationSubsystem.generated.h"

/**
 * 
*/
UCLASS(meta=(DisplayName="SimpleEditorNotificationSubsystem"))
class SIMPLEEDITORNOTIFICATION_API UFTNotificationSubsystem : public UEditorSubsystem
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable)
	/**Notification that allows for a custom icon, passed via its style id name*/
	void NotifyWithCustomIcon(FText Message, float ExpireDuration = 5.0f, FName ImageStyle = FName("Icons.Error"));
	
	UFUNCTION(BlueprintCallable)
	/**Notification that uses the default Icons.Error icon*/
	void NotifyError(FText Message, float ExpireDuration = 5.0f) {NotifyWithCustomIcon(Message, ExpireDuration, FName("Icons.Error"));};
	
	UFUNCTION(BlueprintCallable)
	/**Notification that uses the default Icons.Info icon*/
	void NotifyInfo(FText Message, float ExpireDuration = 5.0f) {NotifyWithCustomIcon(Message, ExpireDuration, FName("Icons.Info"));};
};
